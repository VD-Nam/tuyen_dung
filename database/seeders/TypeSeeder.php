<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('types')->insert([
            'name' => 'Full Time',
        ]);
        DB::table('types')->insert([
            'name' => 'Part Time',
        ]);
        DB::table('types')->insert([
            'name' => 'Tạm thời',
        ]);
    }
}
