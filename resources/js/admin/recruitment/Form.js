import AppForm from '../app-components/Form/AppForm';

Vue.component('recruitment-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                full_name:  '' ,
                phone:  '' ,
                email:  '' ,
                jobs:  '' ,
                introduce:  '' ,
                file_cv:  '' ,
                
            }
        }
    }

});