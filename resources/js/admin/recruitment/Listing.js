import AppListing from '../app-components/Listing/AppListing';

Vue.component('recruitment-listing', {
    mixins: [AppListing]
});