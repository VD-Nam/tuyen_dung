import AppForm from '../app-components/Form/AppForm';

Vue.component('post-form', {
    mixins: [AppForm],
    data: function() {
        return {
            form: {
                name:  '' ,
                summary:  '' ,
                body:  '' ,
                salary:  '' ,
                type_id:  '' ,
                category_id:  '' ,
                deadline:  '' ,
                number:  '' ,
                status:  true ,
            }
        }
    }

});
