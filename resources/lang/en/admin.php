<?php

return [
    'admin-user' => [
        'title' => 'Users',

        'actions' => [
            'index' => 'Users',
            'create' => 'New User',
            'edit' => 'Edit :name',
            'edit_profile' => 'Edit Profile',
            'edit_password' => 'Edit Password',
        ],

        'columns' => [
            'id' => 'ID',
            'last_login_at' => 'Last login',
            'first_name' => 'First name',
            'last_name' => 'Last name',
            'email' => 'Email',
            'password' => 'Password',
            'password_repeat' => 'Password Confirmation',
            'activated' => 'Activated',
            'forbidden' => 'Forbidden',
            'language' => 'Language',
                
            //Belongs to many relations
            'roles' => 'Roles',
                
        ],
    ],

    'post' => [
        'title' => 'Posts',

        'actions' => [
            'index' => 'Posts',
            'create' => 'New Post',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'summary' => 'Summary',
            'content' => 'Content',
            'salary' => 'Salary',
            'type_id' => 'Type',
            'category_id' => 'Category',
            'deadline' => 'Deadline',
            'number' => 'Number',
            'status' => 'Status',
            
        ],
    ],

    'type' => [
        'title' => 'Types',

        'actions' => [
            'index' => 'Types',
            'create' => 'New Type',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            
        ],
    ],

    'category' => [
        'title' => 'Categories',

        'actions' => [
            'index' => 'Categories',
            'create' => 'New Category',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'path_id' => 'Path',
            
        ],
    ],

    'recruitment' => [
        'title' => 'Recruitments',

        'actions' => [
            'index' => 'Recruitments',
            'create' => 'New Recruitment',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'full_name' => 'Full name',
            'phone' => 'Phone',
            'email' => 'Email',
            'jobs' => 'Jobs',
            'introduce' => 'Introduce',
            'file_cv' => 'File cv',
            
        ],
    ],

    'post' => [
        'title' => 'Posts',

        'actions' => [
            'index' => 'Posts',
            'create' => 'New Post',
            'edit' => 'Edit :name',
        ],

        'columns' => [
            'id' => 'ID',
            'name' => 'Name',
            'summary' => 'Summary',
            'body' => 'Body',
            'salary' => 'Salary',
            'type_id' => 'Type',
            'category_id' => 'Category',
            'deadline' => 'Deadline',
            'number' => 'Number',
            'status' => 'Status',
            
        ],
    ],

    // Do not delete me :) I'm used for auto-generation
];